<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_tour_order_user_schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('b_tour_order_id')->unsigned();
            $table->string('title', 191)->nullable();
            $table->string('description', 191)->nullable();
            $table->date('date')->nullable();
            $table->json('detail')->nullable();
            $table->timestamps();
            $table->foreign('b_tour_order_id')->references('id')->on('b_tour_orders')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_tour_order_user_schedules');
    }
};
