<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('category_id')->unsigned();
            $table->string('company_name', 191);
            $table->string('office_name', 191);
            $table->string('map_url', 191);
            $table->string('homepage_url', 500);
            $table->string('manager_email', 191);
            $table->string('tel', 191);
            $table->mediumInteger('status');
            $table->string('start_work', 191);
            $table->string('end_work', 191);
            $table->string('description', 191);
            $table->string('image', 500);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offices');
    }
};
