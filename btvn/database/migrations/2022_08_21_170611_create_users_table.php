<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 500)->nullable();
            $table->string('email', 191)->nullable();
            $table->string('phone', 191)->nullable();
            $table->integer('role');
            $table->tinyInteger('active')->default(1);
            $table->bigInteger('creator_id')->unsigned()->nullable();
            $table->bigInteger('updater_id')->unsigned()->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password', 191)->nullable();
            $table->string('tmp_password', 191)->nullable();
            $table->integer('login_fails')->default(0);
            $table->dateTime('last_login_fail_at')->nullable();
            $table->dateTime('last_login')->nullable();
            $table->string('profile_image', 500)->nullable();
            $table->string('remember_token', 100)->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->bigInteger('member_id')->unsigned()->unique()->nullable();
            $table->foreign('member_id')->references('id')->on('members');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('users');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
};
