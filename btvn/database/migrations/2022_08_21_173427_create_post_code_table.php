<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_code', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('local_code', 191);
            $table->string('old_zip_code', 191);
            $table->string('zip_code', 191);
            $table->string('prefecture_name_kata', 191);
            $table->string('city_name_kata', 191);
            $table->string('town_name_kata', 191);
            $table->string('prefecture_name_kanji', 191);
            $table->string('city_name_kanji', 191);
            $table->string('town_name_kanji', 191);
            $table->integer('display_multiple_zip_code');
            $table->integer('display_small_letter');
            $table->integer('display_town_area');
            $table->integer('display_multiple_town_area');
            $table->integer('display_update');
            $table->integer('display_reason_change');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_code');
    }
};
