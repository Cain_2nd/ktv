<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sei')->nullable();
            $table->string('mei')->nullable();
            $table->string('sei_kana')->nullable();
            $table->string('mei_kana')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->date('birthday')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('address')->nullable();
            $table->bigInteger('prefecture_id')->nullable();
            $table->bigInteger('city_id')->nullable();
            $table->string('street')->nullable();
            $table->string('building')->nullable();
            $table->string('user_type')->nullable();
            $table->tinyInteger('active')->nullable();
            $table->tinyInteger('gender')->nullable();
            $table->string('introducer_name')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('tmp_password')->nullable();
            $table->dateTime('last_login')->nullable();
            $table->integer('login_fails')->default(0);
            $table->dateTime('last_login_fail_at')->nullable();
            $table->string('remember_token')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
};
