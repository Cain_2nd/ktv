<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_tour_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->bigInteger('price');
            $table->string('tourname_name', 191)->nullable();
            $table->text('tourname_explain');
            $table->string('tourname_image', 191)->nullable();
            $table->text('edit_history');
            $table->text('description');
            $table->json('concept')->nullable();
            $table->json('budget')->nullable();
            $table->json('note')->nullable();
            $table->text('contact_content');
            $table->string('contact_image', 500)->nullable();
            $table->string('public_url', 500)->nullable();
            $table->timestamps();
            $table->text('tourname_image_preview');
            $table->text('tourname_image_thumbnail');
            $table->text('contact_image_preview');
            $table->text('contact_image_thumbnail');
            $table->tinyInteger('type')->default(1)->comment('1: Normal, 2: Furusato.');
            $table->bigInteger('furusato_tour_city_id')->nullable();
            $table->string('furusato_code', 191)->nullable();
            $table->text('furusato_from_site', 191)->nullable();
            $table->json('extra_info')->nullable();
            $table->json('concept_user')->nullable();
            $table->json('budget_user')->nullable();
            $table->json('note_user')->nullable();
            $table->text('contact_content_user');
            $table->string('contact_image_user')->nullable();
            $table->string('tour_name_user')->nullable();
            $table->text('tour_explain_user');
            $table->text('tour_image_user');
            $table->string('extra_name')->nullable();
            $table->text('extra_image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Schema::dropIfExists('b_tour_orders');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
};
