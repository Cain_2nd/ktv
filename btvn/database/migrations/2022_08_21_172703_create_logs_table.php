<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('object_id')->unsigned();
            $table->bigInteger('owner_id')->unsigned();
            $table->integer('category');
            $table->integer('action');
            $table->text('info');
            $table->mediumText('before');
            $table->mediumText('before_changes');
            $table->mediumText('after');
            $table->mediumText('after_changes');
            $table->timestamps();
            $table->index('category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
};
