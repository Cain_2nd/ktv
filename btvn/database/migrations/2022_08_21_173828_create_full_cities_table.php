<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('full_cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('prefecture_id')->unsigned();
            $table->string('name', 191);
            $table->string('code', 191);
            $table->timestamps();
            $table->foreign('prefecture_id')->references('id')->on('prefectures')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('full_cities');
    }
};
