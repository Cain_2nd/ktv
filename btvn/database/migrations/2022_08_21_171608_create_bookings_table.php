<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('member_id')->unsigned();
            $table->bigInteger('b_tour_order_id')->unsigned()->nullable();
            $table->bigInteger('tour_order_template_id')->nullable();
            $table->bigInteger('admin_id')->unsigned()->nullable();
            $table->integer('discuss_method')->nullable();
            $table->json('discuss_time')->nullable();
            $table->date('hearing_first_time')->nullable();
            $table->text('hearing_tour_purpose');
            $table->string('hearing_budget', 191)->nullable();
            $table->string('hearing_plan_time', 191)->nullable();
            $table->string('hearing_area', 191)->nullable();
            $table->bigInteger('adult_count');
            $table->bigInteger('child_count');
            $table->text('hearing_room_allocation');
            $table->text('hearing_meal_note');
            $table->text('hearing_tour_note');
            $table->text('hearing_other_note');
            $table->text('introduce_info');
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->date('application_deadline')->nullable();
            $table->integer('proposal_status')->nullable();
            $table->integer('invoice_status')->nullable();
            $table->date('payment_deadline')->nullable();
            $table->integer('payment_status')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->date('payment_date')->nullable();
            $table->bigInteger('payment_confirm_admin_id')->unsigned()->nullable();
            $table->tinyInteger('is_receive_info')->nullable();
            $table->tinyInteger('type')->default(1)->comment('1: Normal, 2: Furusato.');
            $table->text('note');
            $table->json('information_create_plan')->nullable();
            $table->json('information_verify_plan')->nullable();
            $table->json('information_draft_booking')->nullable();
            $table->json('information_booking')->nullable();
            $table->bigInteger('user_confirmed_id')->nullable();
            $table->tinyInteger('first_time_support_review_status')->default(1)->comment('1: Reviewing, 2 finish_review');
            $table->tinyInteger('input_hearing_sheet_review_status')->default(1)->comment('1: Reviewing, 2 finish_review');
            $table->tinyInteger('input_tour_plan_review_status')->default(1)->comment('1: Reviewing, 2 finish_review');
            $table->tinyInteger('final_information_review_status')->default(1)->comment('1: Reviewing, 2 finish_review');
            $table->tinyInteger('want_to_mail')->default(0);
            $table->dateTime('lock_paypal_checkout')->nullable();
            $table->bigInteger('adult_count_tour')->nullable();
            $table->bigInteger('child_count_tour')->nullable();
            $table->dateTime('consultation_date')->nullable();
            $table->tinyInteger('budget_remark_status')->default(1);
            $table->text('budget_remark');
            $table->foreign('admin_id')->references('id')->on('users')->onUpdate('cascade');
            $table->foreign('b_tour_order_id')->references('id')->on('b_tour_orders')->onUpdate('cascade');
            $table->foreign('member_id')->references('id')->on('members')->onUpdate('cascade');
            $table->foreign('payment_confirm_admin_id')->references('id')->on('users')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
};
